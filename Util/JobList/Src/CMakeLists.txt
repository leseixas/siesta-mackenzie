# To avoid "multiple rules" in Ninja
# Maybe turn into global-utility targets

add_library(aux_joblist     jobList.f90 posix_calls.f90)

add_executable(countJobs  countJobs.f90 )
add_executable(runJobs    runJobs.f90 )
add_executable(getResults getResults.f90 )
add_executable(horizontal horizontal.f90 )

target_link_libraries(countJobs aux_joblist)
target_link_libraries(runJobs aux_joblist)
target_link_libraries(getResults aux_joblist)
target_link_libraries(horizontal aux_joblist)
		     
install(TARGETS countJobs runJobs getResults horizontal RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR} )
#---------------------------------------------------------------------
