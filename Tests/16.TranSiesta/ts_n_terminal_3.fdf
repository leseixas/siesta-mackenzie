SystemLabel ts_n_terminal_3

NumberOfAtoms 43
NumberOfSpecies 1

%block ChemicalSpeciesLabel
 1  6  C
%endblock ChemicalSpeciesLabel

LatticeConstant 1.0 Ang
%block LatticeVectors
    31.75        0.0000      0.0000
     0.0000     12.0000      0.0000
     0.0000      0.0000     31.75
%endblock LatticeVectors

%block AtomicCoordinatesOrigin
  1. 1. 1.
%endblock

AtomicCoordinatesFormat Ang
%block AtomicCoordinatesAndAtomicSpecies
  0.0  5.0  15.24 1
  1.27  5.0  15.24 1
  2.54  5.0  15.24 1
  3.81  5.0  15.24 1
  5.08  5.0  15.24 1
  6.35  5.0  15.24 1
  7.62  5.0  15.24 1
  8.89  5.0  15.24 1
  10.16  5.0  15.24 1
  11.43  5.0  15.24 1
  12.7  5.0  15.24 1
  13.97  5.0  15.24 1
  15.24  5.0  0.0 1
  15.24  5.0  1.27 1
  15.24  5.0  2.54 1
  15.24  5.0  3.81 1
  15.24  5.0  5.08 1
  15.24  5.0  6.35 1
  15.24  5.0  7.62 1
  15.24  5.0  8.89 1
  15.24  5.0  10.16 1
  15.24  5.0  11.43 1
  15.24  5.0  12.7 1
  15.24  5.0  13.97 1
  15.24  5.0  16.51 1
  15.24  5.0  17.78 1
  15.24  5.0  19.05 1
  15.24  5.0  20.32 1
  15.24  5.0  21.59 1
  15.24  5.0  22.86 1
  15.24  5.0  24.13 1
  15.24  5.0  25.4 1
  15.24  5.0  26.67 1
  15.24  5.0  27.94 1
  15.24  5.0  29.21 1
  15.24  5.0  30.48 1
  16.51  5.0  15.24 1
  17.78  5.0  15.24 1
  19.05  5.0  15.24 1
  20.32  5.0  15.24 1
  21.59  5.0  15.24 1
  22.86  5.0  15.24 1
  24.13  5.0  15.24 1
%endblock AtomicCoordinatesAndAtomicSpecies

PAO.BasisSize         SZ
PAO.EnergyShift       275 meV

MeshCutoff              50. Ry
XC.functional           LDA
XC.authors              PZ
SolutionMethod          transiesta


MinSCFIterations       3
MaxSCFIterations       200
DM.MixingWeight        0.05
DM.Tolerance           0.0001
DM.NumberPulay         6
DM.UseSaveDM           .true.
DM.MixSCF1             .true.

WriteMullikenPop                1
WriteDM                         T
WriteForces                     T
SaveHS                          T


# Transiesta options
%block TS.kgrid_monkhorst_pack
 1 0 0 0.
 0 1 0 0.
 0 0 1 0.
%endblock
TS.BTD.Pivot atom+GGPS
TS.Hartree.Fix -C+C

%block TS.ChemPots
  high
  low
%endblock TS.ChemPots
%block TS.ChemPot.high
  mu V
  contour.eq
    begin
      C-high
      T-high
    end
%endblock TS.ChemPot.high
%block TS.ChemPot.low
  mu 0. eV
  contour.eq
    begin
      C-low
      T-low
    end
%endblock TS.ChemPot.low

%block TS.Contour.C-high
  part circle
   from -40. eV + V to -10 kT + V
     points 34
      method g-legendre
%endblock TS.Contour.C-high
%block TS.Contour.T-high
  part tail
   from prev to inf
     points 12
      method g-fermi
%endblock TS.Contour.T-high
%block TS.Contour.C-low
  part circle
   from -40. eV to -10 kT
     points 34
      method g-legendre
%endblock TS.Contour.C-low
%block TS.Contour.T-low
  part tail
   from prev to inf
     points 12
      method g-fermi
%endblock TS.Contour.T-low

%block TS.Contours.nEq
  neq
%endblock TS.Contours.nEq
%block TS.Contour.nEq.neq
  part line
   from 0 - 5 kT to |V| + 5 kT
     delta 0.01 eV
      method mid-rule
%endblock TS.Contour.nEq.neq

%block TS.Elecs
  el-1
  el-3
  el-4
%endblock TS.Elecs

%block TS.Elec.el-1
  TSHS ./ts_n_terminal_elec_x.TSHS
  chemical-potential high
  semi-inf-direction -a1
  electrode-position 1
%endblock TS.Elec.el-1
%block TS.Elec.el-3
  TSHS ./ts_n_terminal_elec_z.TSHS
  chemical-potential low
  semi-inf-direction -a1
  electrode-position 13
%endblock TS.Elec.el-3
%block TS.Elec.el-4
  TSHS ./ts_n_terminal_elec_z.TSHS
  chemical-potential high
  semi-inf-direction +a1
  electrode-position end -8
%endblock TS.Elec.el-4

%block TBT.k
  diag 1 1 1
%endblock
TBT.DOS.A T
TBT.DOS.Gf T

%block TBT.Contours
    line
%endblock TBT.Contours
%block TBT.Contour.line
  part line
     from -15. eV to 10. eV
      delta 0.01 eV
        method mid-rule
%endblock TBT.Contour.line
